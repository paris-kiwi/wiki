me=$(shell whoami)

kiwi_prod_home=/usr/share/webapps/mediawiki
kiwi_prod_themes_home=${kiwi_prod_home}/skins
kiwi_prod_extensions_home=${kiwi_prod_home}/extensions
kiwi_dev_home=/home/${me}/project/paris-kiwi
kiwi_dev_themes_home=${kiwi_dev_home}/skins
kiwi_dev_extensions_home=${kiwi_dev_home}/extensions

today=$(shell date +%Y-%m-%d-%H%M%S)

copy-calendar-to-git:
	@echo "Copying Calendar extension to ${kiwi_dev_home}"
	@cp -pr ${kiwi_prod_extensions_home}/Calendar/. ${kiwi_dev_extensions_home}

package:

tests: home-test date-test

home-test:
	echo ${kiwi_prod_home}
	echo ${kiwi_prod_themes_home}
	echo ${kiwi_prod_extensions_home}
	echo ${kiwi_dev_home}
	echo ${kiwi_dev_themes_home}
	echo ${kiwi_dev_extensions_home}

date-test:
	echo ${today}
