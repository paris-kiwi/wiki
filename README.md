paris-kiwi
==========

Un calendrier de concerts et d'infos du DIY sur Paris & sa banlieue.

Réalisé avec Mediawiki et l'extension
[mw-calendar](https://github.com/mediawiki-extensions/mw-calendar).

C'est quoi ?
============

Sa vocation est de répertorier les pratiques, les groupes, les collectifs, mais aussi de communiquer de manière alternative et autonome sur des évènements. Nous sommes un site non marchand s'opposant à toute forme de domination, foncièrement anti-capitaliste, anti-sexiste, anti-homophobe et anti-raciste. Si vous utilisez le ParisKiwi, n'oubliez pas que c'est pour créer du sens et pas pour reproduire la merde mainstream. Nous n'avons pas vocation à devenir une plateforme de promotion pour des salles, groupes ou collectifs qui ne s'intéressent pas à créer une alternative.

A quoi ça sert ?
================

Vous pourrez vous renseigner sur les Concerts, les Collectifs, les Groupes et consulter en ligne la newsletter Pollution Capitale.

Comment ça marche ?
==================

Le site se présente comme un Wiki (Wikipedia est un Wiki), vous pouvez donc compléter les pages en postant vos dates de Concerts, etc. Dès que vous voyez un mot/lien rouge, c'est que la page n'a pas été commencée, vous pouvez cliquer dessus et vous y mettre. Lorsque vous aurez créé cette page, le lien devient bleu, ce qui signifie qu'il mène à une page. Le but est de regrouper un maximum d'informations sur ce qui se fait de chouette à Paris, cela inclut donc les Lieux de concerts, les Disquaires et les Librairies, mais certainement encore bien d'autres choses qu'il nous reste à définir.

Libre et open-source ?
======================

Nous sommes ouverts à toute participation à l'amélioration de ce site, notamment si vous nous dites Ce qui merde, mais aussi dans la vraie vie si vous voulez donner un coup de main dans les concerts ou autre...

En 2017, nous reprenons la maintenance de l'extension Calendar afin d'en corriger quelques bugs et de la rendre compatible avec les dernières versions de Mediawiki (son mainteneur kenyu73 ne la maintenant plus depuis *2009*).
